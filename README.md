# wayland-cr

Wayland bindings for Crystal

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     wayland-cr:
       git: https://codeberg.org/uncomfyhalomacro/wayland-cr   
   ```

2. Run `shards install`

## Usage

```crystal
require "wayland"

module YourModule
  include Wayland
end
```

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://codeberg.org/repo/fork/158210>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Soc Virnyl Estela](https://codeberg.org/uncomfyhalomacro) - creator and maintainer
